#get tomcat now
FROM tomcat

RUN apt-get update && apt-get clean
COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/scalatra.war

EXPOSE 8080

CMD ["catalina.sh","run"]
